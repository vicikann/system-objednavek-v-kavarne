<h3>Aktuální dokumenty</h3>

<h4>[Aktuální prezentace](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Prezentace/finalni_prezentace_13_05_2019.pdf)</h4>

<h4>[Finální dokumentace (verze 8)](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/finalni_dokumentace_04.pdf)</h4>

<h4>[Oponentura pro tým č.4: Finální dokumentace](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Oponentura/Oponentura_pro_4__Fin%C3%A1ln%C3%AD_dokumentace.pdf)</h4>

<h4>[Klikatelný prototyp (Axure)](https://tm0u86.axshare.com/#id=c4rgyk&p=prihlaseni&g=1)</h4>

<h3>Ostatní dokumenty</h3>

<strong>Byznys analýza </strong>
[Google Docs](https://docs.google.com/document/d/1CzIB85sX_dmw_7Y1XkM-fxsC6uuv2B7lVFwTBKRFPWo/edit?usp=sharing),  [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Byznys_anal%C3%BDza_-_fin%C3%A1ln%C3%AD_verze.pdf) 

<strong>Business Domain Model</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Business_Domain_Model.pdf)

<strong>Procesní model</strong> [Zákazník a obsluha](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/BPM-Zakaznik_Obsluha.png) |
[Naskladnění](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/BPM-Naskladneni.png)


<strong>Byznys požadavky</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Business_po%C5%BEadavky01.pdf)

<strong>Varianty implementace</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Varianty_implementace.pdf)

<strong>Analytický doménový model</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Analytic_Domain_Model.pdf)

<strong>Systémové požadavky</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Syst%C3%A9mov%C3%A9_po%C5%BEadavky.pdf) | [Příloha: Tabulka systémových požadavků a business požadavků](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Syst%C3%A9mov%C3%A9_po%C5%BEadavky_-_tabulka_-_List_1.pdf)

<strong>Model komponent</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Model_komponent.png)

<strong>Sekvenční diagramy</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Sekvencni_diagram.pdf)

<strong>Stavové diagramy</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Stavove_diagramy.pdf)

<strong>Model nasazení</strong> [PNG](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Model_nasazeni.png)

<strong>Use case model</strong> [PDF](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Use-Cases.pdf)

<strong>Wireframe</strong> [PNG](https://gitlab.fel.cvut.cz/vicikann/system-objednavek-v-kavarne/blob/master/Dokumenty/Wireframe.png)


